import '../../data/model/git_repository_response_model.dart';

abstract class GitRepositoryRepository {
  Future<GitRepositoryResponseModel> repository();
}
