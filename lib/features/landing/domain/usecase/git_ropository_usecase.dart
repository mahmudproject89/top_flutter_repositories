import '../../data/model/git_repository_response_model.dart';
import '../repository/git_repository_repository.dart';

class GitRepositoryUsecase {
  final GitRepositoryRepository _gitRepositoryRepository;

  GitRepositoryUsecase(this._gitRepositoryRepository);

  Future<GitRepositoryResponseModel> call() =>
      _gitRepositoryRepository.repository();
}
