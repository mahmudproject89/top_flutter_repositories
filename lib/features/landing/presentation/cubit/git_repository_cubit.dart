// ignore_for_file: empty_catches

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/model/git_repository_response_model.dart';
import '../../domain/usecase/git_ropository_usecase.dart';

part 'git_repository_state.dart';

class GitRepositoryCubit extends Cubit<GitRepositoryState> {
  final GitRepositoryUsecase gitRepositoryUsecase;

  GitRepositoryCubit({
    required this.gitRepositoryUsecase,
  }) : super(GitRepositoryInitial());

  Future<void> getGitRepository() async {
    try {
      emit(GitRepositoryLoading());

      final responseModel = await gitRepositoryUsecase();

      emit(GitRepositorySucceed(model: responseModel));
    } catch (ex, strackTrace) {
      emit(GitRepositoryFailed(ex, strackTrace));
    }
  }
}
