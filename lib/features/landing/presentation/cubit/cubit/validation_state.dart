// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'validation_cubit.dart';

class ValidationState extends Equatable {
  final List<Item> itemList;

  const ValidationState({
    required this.itemList,
  });

  ValidationState copyWith({
    final List<Item>? itemList,
  }) {
    return ValidationState(
      itemList: itemList ?? this.itemList,
    );
  }

  @override
  List<Object> get props => [
        itemList,
      ];
}

final class ValidationInitial extends ValidationState {
  const ValidationInitial({required super.itemList});
}
