// ignore_for_file: empty_catches

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:oktoast/oktoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../core/utils/nums.dart';
import '../../../data/model/git_repository_response_model.dart';

part 'validation_state.dart';

class ValidationCubit extends Cubit<ValidationState> {
  ValidationCubit() : super(ValidationInitial(itemList: []));



  saveSelectedOption(SortingOption option) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('sortingOption', option.index);
    selectedOption = option;
  }

  Future<void> addItemList(List<Item> items) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    selectedOption = SortingOption.values[prefs.getInt('sortingOption') ?? 0];
    final updatedItemList = List<Item>.from(state.itemList);
    updatedItemList.addAll(items);
    updatedItemList.sort((a, b) {
      switch (selectedOption) {
        case SortingOption.byDateTime:
          return b.updatedAt!.compareTo(a.updatedAt!);
        case SortingOption.byStarCount:
          return b.stargazersCount!.compareTo(a.stargazersCount!);
      }
    });
    
    emit(state.copyWith(itemList: updatedItemList));
  }

  Future<void> offlineItemList(List<Item> items) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    selectedOption = SortingOption.values[prefs.getInt('sortingOption') ?? 0];
    final updatedItemList = <Item>[];
    updatedItemList.addAll(items);
    updatedItemList.sort((a, b) {
      switch (selectedOption) {
        case SortingOption.byDateTime:
          return b.updatedAt!.compareTo(a.updatedAt!);
        case SortingOption.byStarCount:
          return b.stargazersCount!.compareTo(a.stargazersCount!);
      }
    });
    
    emit(state.copyWith(itemList: updatedItemList));
  }

  Future<void> sortItemList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    selectedOption = SortingOption.values[prefs.getInt('sortingOption') ?? 0];

    final updatedItemList = List<Item>.from(state.itemList);
    updatedItemList.sort((a, b) {
      switch (selectedOption) {
        case SortingOption.byDateTime:
          return b.updatedAt!.compareTo(a.updatedAt!);
        case SortingOption.byStarCount:
          return b.stargazersCount!.compareTo(a.stargazersCount!);
      }
    });
    emit(state.copyWith(itemList: updatedItemList));
  }



  DateTime? _lastRefreshTime; // Maintain timestamp of last refresh

  Future<void> refreshData() async {
    if (_lastRefreshTime == null ||
        DateTime.now().difference(_lastRefreshTime!) >=
            const Duration(minutes: 30)) {
      try {
        _lastRefreshTime = DateTime.now(); // Update last refresh timestamp
      } catch (e) {}
    } else {
      if (_getTimeLeftMsg().isNotEmpty) {
        showToast(_getTimeLeftMsg(), position: ToastPosition.bottom);
      }
    }
  }

  String _getTimeLeftMsg() {
    if (_lastRefreshTime != null) {
      DateTime nextRefreshTime =
          _lastRefreshTime!.add(const Duration(minutes: 30));
      Duration timeLeft = nextRefreshTime.difference(DateTime.now());
      int minutesLeft = timeLeft.inMinutes % 60;
      return 'Next refresh possible after $minutesLeft minutes later';
    } else {
      return '';
    }
  }
}
