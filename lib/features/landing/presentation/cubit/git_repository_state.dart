part of 'git_repository_cubit.dart';

abstract class GitRepositoryState extends Equatable {
  const GitRepositoryState();

  @override
  List<Object> get props => [];
}

class GitRepositoryInitial extends GitRepositoryState {}

class GitRepositoryLoading extends GitRepositoryState {}

class GitRepositoryFailed extends GitRepositoryState {
  final StackTrace stackTrace;
  final Object exception;

  const GitRepositoryFailed(this.exception, this.stackTrace);
}

class GitRepositorySucceed extends GitRepositoryState {
  final GitRepositoryResponseModel model;

  const GitRepositorySucceed({
    required this.model,
  });
}

