// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, use_build_context_synchronously

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

import '../../../../core/exceptions/exceptions.dart';
import '../../../../core/network/connection_checker.dart';
import '../../../../core/resources/error_msg_res.dart';
import '../../../../core/utils/nums.dart';
import '../../../common/presentation/widgets/app_bar_widget.dart';
import '../../../common/presentation/widgets/app_dialog.dart';
import '../../data/database/database_helper.dart';
import '../../data/model/git_repository_response_model.dart';
import '../cubit/cubit/validation_cubit.dart';
import '../cubit/git_repository_cubit.dart';
import '../widgets/repo_item.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  final controller = ScrollController();
  DatabaseHelper databaseHelper = DatabaseHelper.instance;
  @override
  void initState() {
    super.initState();
    getGitRepo();
    controller.addListener(() {
      if (controller.position.maxScrollExtent == controller.offset) {
        page++;
        fetch();
      }
    });
  }

  getGitRepo() async {
    isConnected = await checkInternetConnection();

    if (isConnected == true) {
      await BlocProvider.of<GitRepositoryCubit>(context).getGitRepository();
    } else {
      AppDialog(
        title: ErrorMsgRes.kNoInternet,
        isNoInternet: true,
      );
      List<Item> nItems = await databaseHelper.getItems();
      context.read<ValidationCubit>().offlineItemList(nItems);
    }
  }

  Future fetch() async {
    getGitRepo();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  insertDB(newItem) async {
    await databaseHelper.insertItems(newItem);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<GitRepositoryCubit, GitRepositoryState>(
      listener: (context, state) async {
        if (state is GitRepositoryLoading) {
          Center(
            child: LoadingAnimationWidget.discreteCircle(
              color: Colors.white.withOpacity(0.25),
              size: 50,
            ),
          );
        } else if (state is GitRepositorySucceed) {
          final nItems = state.model.items ?? [];

          insertDB(nItems);
          context.read<ValidationCubit>().addItemList(nItems);
        } else if (state is GitRepositoryFailed) {
          // Navigator.pop(context);
          final ex = state.exception;
          if (ex is ServerException) {
            AppDialog(
              title: ex.message ?? '',
              isNoInternet: false,
            );
            //  return showAppDialog(context, title: ex.message ?? '');
          } else if (ex is NoInternetException) {
            AppDialog(
              title: ErrorMsgRes.kNoInternet,
              isNoInternet: true,
            );
          } else {
            Text('No data found!');
          }
        }
      },
      child: Scaffold(
          appBar: AppBarWidget(
            title: 'tfr',
            isActionButton: true,
          ),
          body: RefreshIndicator(
              onRefresh: () async {
                await BlocProvider.of<ValidationCubit>(context).refreshData();
              },
              child: repoItemList())),
    );
  }

  Widget repoItemList() {
    return BlocBuilder<ValidationCubit, ValidationState>(
      builder: (context, state) {
        final items = state.itemList;
        return ListView.separated(
          controller: controller,
          padding: EdgeInsets.symmetric(horizontal: 16),
          itemCount: items.length + 1,
          separatorBuilder: (context, index) => const SizedBox(
            height: 10,
          ),
          // physics: const BouncingScrollPhysics(),
          itemBuilder: (context, index) {
            if (index < items.length) {
              return RepoItem(
                items: items[index],
              );
            } else {
              if (isConnected == true) {
                return const Padding(
                  padding: EdgeInsets.symmetric(vertical: 32),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              } else {
                return Center(
                  child: Text(
                    ErrorMsgRes.kNoInternet,
                  ),
                );
              }
            }
          },
        );
      },
    );
  }
}
