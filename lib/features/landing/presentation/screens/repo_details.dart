import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../core/utils/size_config.dart';
import '../../../common/presentation/widgets/app_bar_widget.dart';
import '../../data/model/git_repository_response_model.dart';

class RepoDetails extends StatelessWidget {
  const RepoDetails({
    Key? key,
    required this.item,
  }) : super(key: key);
  final Item item;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: const AppBarWidget(
        title: 'Repository Details',
        isBackButton: true,
        isActionButton: false,
      ),

      //  AppBarWidget(title: 'Repo Details'),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildItemTitle(),
            _buildItemImage(),
            _buildItemDescription(),
            _buildItemForkUpdate()
          ],
        ),
      ),
    );
  }

  Widget _buildItemTitle() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            item.name ?? '',
            style: const TextStyle(
              fontFamily: 'Butler',
              fontSize: 20,
              fontWeight: FontWeight.w900,
            ),
          ),
          const SizedBox(height: 5), // Add spacing between title and icon
          Row(
            children: [
              const Icon(
                Icons.star_border_outlined,
                color: Colors.yellow,
              ),
              const SizedBox(width: 5),
              Text(
                '${item.stargazersCount}',
                style: const TextStyle(
                  fontSize: 15,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildItemImage() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 18),
      child: Container(
        height: 250,
        margin: const EdgeInsets.only(top: 14),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), // Add border radius
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: CachedNetworkImage(
            imageUrl: item.avatarUrl ?? '',
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            placeholder: (context, url) => const CircularProgressIndicator(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        ),
      ),
    );
  }

  Widget _buildItemDescription() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 18),
      child: Text(
        '${item.description}\n\n${item.topics}',
        style: const TextStyle(fontSize: 16),
      ),
    );
  }

  Widget _buildItemForkUpdate() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Image.asset(
                'assets/images/forks.png',
                height: 16,
              ),

              // Icon(Icons.fork),
              const SizedBox(width: 5),
              Text('${item.forks} forks'),
            ],
          ),
          Row(
            children: [
              const Icon(Icons.update),
              const SizedBox(width: 5),
              Text(DateFormat('MM-dd-yy HH:mm.ss').format(item.updatedAt!)),
            ],
          ),
        ],
      ),
    );
  }
}
