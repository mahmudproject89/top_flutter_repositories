import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../data/model/git_repository_response_model.dart';
import '../screens/repo_details.dart';

class RepoItem extends StatefulWidget {
  const RepoItem({
    Key? key,
    required this.items,
  }) : super(key: key);

  final Item items;

  @override
  State<RepoItem> createState() => _RepoItemState();
}

class _RepoItemState extends State<RepoItem> {
  @override
  Widget build(BuildContext context) => ListTile(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => RepoDetails(item: widget.items),
          ),
        ),
        contentPadding: EdgeInsets.zero,
        leading: Stack(
          alignment: Alignment.bottomRight,
          children: [
            CircleAvatar(
                radius: 30,
                backgroundImage:
                    CachedNetworkImageProvider(widget.items.avatarUrl!)
                ),
          ],
        ),
        title: Text(
          widget.items.name!,
          style: const TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Column(
          children: [
            Text(
              'Last Update: ${DateFormat('MM-dd-yyyy HH:mm.ss').format(widget.items.updatedAt!)}',
              maxLines: 2,
              style: const TextStyle(
                fontSize: 15,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Row(
              children: [
                const Icon(
                  Icons.star_border_outlined,
                  color: Colors.yellow,
                ), // Replace 'Icons.access_time' with your desired icon
                const SizedBox(
                    width:
                        5), // Adjust the spacing between the icon and text as needed
                Expanded(
                  child: Text(
                    '${(widget.items.stargazersCount)}',
                    maxLines: 2,
                    style: const TextStyle(
                      fontSize: 15,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
}
