class GitRepositoryResponseModel {
  List<Item>? items;

  GitRepositoryResponseModel({
    this.items,
  });

  factory GitRepositoryResponseModel.fromJson(Map<String, dynamic> json) {
    return GitRepositoryResponseModel(
      items: (json['items'] as List<dynamic>?)
          ?.map((itemJson) => Item.fromJson(itemJson))
          .toList(),
    );
  }
}

class Item {
  int? id;
  String? name;
  String? fullName;
  String? avatarUrl;
  String? description;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? pushedAt;
  int? stargazersCount;
  int? forksCount;
  int? forks;
  List<String>? topics;

  Item({
    this.id,
    this.name,
    this.fullName,
    this.avatarUrl,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.pushedAt,
    this.stargazersCount,
    this.forksCount,
    this.forks,
    this.topics,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        name: json["name"],
        fullName: json["full_name"],
        avatarUrl: json['owner']['avatar_url'],
        description: json["description"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        pushedAt: json["pushed_at"] == null
            ? null
            : DateTime.parse(json["pushed_at"]),
        stargazersCount: json["stargazers_count"],
        forksCount: json["forks_count"],
        forks: json["forks"],
        topics: json["topics"] == null
            ? []
            : List<String>.from(json["topics"]!.map((x) => x)),
      );



  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'fullName': fullName,
      'avatarUrl': avatarUrl,
      'description': description,
      'createdAt': createdAt?.toIso8601String(),
      'updatedAt': updatedAt?.toIso8601String(),
      'pushedAt': pushedAt?.toIso8601String(),
      'stargazersCount': stargazersCount,
      'forksCount': forksCount,
      'forks': forks,
      'topics': topics?.join(','),
    };
  }

  factory Item.fromMap(Map<String, dynamic> map) {
    return Item(
      id: map['id'],
      name: map['name'],
      fullName: map['fullName'],
      avatarUrl: map['avatarUrl'],
      description: map['description'],
      createdAt: map['createdAt'] == null ? null : DateTime.parse(map['createdAt']),
      updatedAt: map['updatedAt'] == null ? null : DateTime.parse(map['updatedAt']),
      pushedAt: map['pushedAt'] == null ? null : DateTime.parse(map['pushedAt']),
      stargazersCount: map['stargazersCount'],
      forksCount: map['forksCount'],
      forks: map['forks'],
      topics: map['topics']?.split(','),
    );
  }
}
