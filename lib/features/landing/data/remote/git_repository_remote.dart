import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../../core/constant/api_constants.dart';
import '../../../../core/exceptions/exceptions.dart';
import '../../../../core/header_provider/header_provider.dart';
import '../../../../core/resources/error_msg_res.dart';
import '../../../../core/utils/nums.dart';
import '../model/git_repository_response_model.dart';

abstract class GitRepositoryRemote {
  Future<GitRepositoryResponseModel> repository();
}

class GitRepositoryRemoteImpl implements GitRepositoryRemote {
  static const repositoryEndpoint =
      ApiConstants.baseApiUrl + ApiConstants.searchUrl;
  final HeaderProvider _headerProvider;

  GitRepositoryRemoteImpl(this._headerProvider);
  var url = Uri.parse(repositoryEndpoint);

  @override
  Future<GitRepositoryResponseModel> repository() async {
    GitRepositoryResponseModel res;

    var queryParams = {'q': 'flutter', 'page': '$page', 'per_page': '10'};
    url = url.replace(queryParameters: queryParams);

    final headers = _headerProvider();

    final response = await http.get(url, headers: headers);

    if (response.statusCode == 200) {
      final jsonBody = jsonDecode(response.body);
      res = GitRepositoryResponseModel.fromJson(jsonBody);

      return res;
    } else if (response.statusCode == 204) {
      throw ServerException(
        message: ErrorMsgRes.kNoContent,
        statusCode: response.statusCode,
      );
    } else if (response.statusCode == 400) {
      throw ServerException(
        message: ErrorMsgRes.kBadRequest,
        statusCode: response.statusCode,
      );
    } else if (response.statusCode == 401) {
      throw ServerException(
        message: ErrorMsgRes.kUnauthorized,
        statusCode: response.statusCode,
      );
    } else if (response.statusCode == 404) {
      throw ServerException(
        message: ErrorMsgRes.kNotFound,
        statusCode: response.statusCode,
      );
    } else if (response.statusCode == 408) {
      throw ServerException(
        message: ErrorMsgRes.kRequestTimeOut,
        statusCode: response.statusCode,
      );
    } else if (response.statusCode == 431) {
      throw ServerException(
        message: ErrorMsgRes.kTooManyRequest,
        statusCode: response.statusCode,
      );
    } else if (response.statusCode == 500) {
      throw ServerException(
        message: ErrorMsgRes.kServerError,
        statusCode: response.statusCode,
      );
    } else if (response.statusCode == 503) {
      throw ServerException(
        message: ErrorMsgRes.kServiceUnavailable,
        statusCode: response.statusCode,
      );
    } else {
      throw ServerException(
        message: response.body,
        statusCode: response.statusCode,
      );
    }
  }
}
