import '../../../../core/exceptions/exceptions.dart';
import '../../../../core/network/connection_checker.dart';
import '../../domain/repository/git_repository_repository.dart';
import '../model/git_repository_response_model.dart';
import '../remote/git_repository_remote.dart';

class GitRepositoryRepositoryImpl implements GitRepositoryRepository {
  final ConnectionChecker connectionChecker;
  final GitRepositoryRemote gitRepositoryRemote;

  GitRepositoryRepositoryImpl(
    this.connectionChecker,
    this.gitRepositoryRemote,
  );

  @override
  Future<GitRepositoryResponseModel> repository() async {
    if (!await connectionChecker.isConnected()) throw NoInternetException();
    GitRepositoryResponseModel gitRepositoryResponse =
        await gitRepositoryRemote.repository();
    return gitRepositoryResponse;
  }
}
