import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../model/git_repository_response_model.dart';

class DatabaseHelper {
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;

  DatabaseHelper._privateConstructor();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    String path = join(await getDatabasesPath(), 'repositories.db');
    return await openDatabase(path, version: 1, onCreate: _createDb);
  }

  Future<void> _createDb(Database db, int version) async {
    await db.execute('''
      CREATE TABLE items(
        id INTEGER PRIMARY KEY,
        name TEXT,
        fullName TEXT,
        avatarUrl TEXT,
        description TEXT,
        createdAt TEXT,
        updatedAt TEXT,
        pushedAt TEXT,
        stargazersCount INTEGER,
        forksCount INTEGER,
        forks INTEGER,
        topics TEXT
      )
    ''');
  }

  Future<int> insertItem(Item item) async {
    Database db = await instance.database;
    return await db.insert('items', item.toMap());
  }
  
  Future<void> insertItems(List<Item> items) async {
  Database db = await instance.database;
  Batch batch = db.batch();
  for (var item in items) {
    batch.insert('items', item.toMap());
  }
  await batch.commit();
}


  Future<List<Item>> getItems() async {
    Database db = await instance.database;
    List<Map<String, dynamic>> maps = await db.query('items');
    return List.generate(maps.length, (i) {
      return Item.fromMap(maps[i]);
    });
  }
  
  Future<int> deleteAllItems() async {
    Database db = await instance.database;
    return await db.delete('items');
  }
}
