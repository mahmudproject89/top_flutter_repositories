import 'package:flutter/material.dart';
// import '../../features/common/presentation/screens/splash_screen.dart';
import '../../features/landing/presentation/screens/home_page.dart';
import 'route_name.dart';

class RouteConfig {
  Route routes(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case RouteName.initialRoute:
        return _getInitialRoute();
      case RouteName.homepageRoute:
        return _getHomepageRoute();
    }
    return _defaultRoute();
  }

  static MaterialPageRoute _routeBuilder(Widget child) {
    return MaterialPageRoute(builder: (_) => child);
  }

  static MaterialPageRoute _defaultRoute() {
    return _routeBuilder(
      const Scaffold(
        body: Center(
          child: Text('Unknown route'),
        ),
      ),
    );
  }

  static MaterialPageRoute _getInitialRoute() {
    return _routeBuilder(const HomePage());
    // return _routeBuilder(const SplashScreen());
  }

  static MaterialPageRoute _getHomepageRoute() {
    return _routeBuilder(const HomePage());
  }
}
