class ApiConstants {
  ApiConstants._();

  static const baseApiUrl = 'https://api.github.com';
  static const searchUrl = '/search/repositories';
}
