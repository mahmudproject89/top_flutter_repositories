
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_flutter_repositories/features/landing/presentation/cubit/cubit/validation_cubit.dart';

import '../../features/common/data/repository_impl/locale_repo_impl.dart';
import '../../features/common/domain/repository/locale_repository.dart';
import '../../features/common/domain/usecase/locale_usecase.dart';
import '../../features/common/presentation/cubit/locale/locale_cubit.dart';
import '../../features/common/data/data_source/local/locale_source.dart';
import '../../features/common/data/data_source/local/token_source.dart';
import '../../features/landing/data/remote/git_repository_remote.dart';
import '../../features/landing/data/repository_impl/git_repository_repository_impl.dart';
import '../../features/landing/domain/repository/git_repository_repository.dart';
import '../../features/landing/domain/usecase/git_ropository_usecase.dart';
import '../../features/landing/presentation/cubit/git_repository_cubit.dart';
import '../header_provider/header_provider.dart';

import '../network/connection_checker.dart';

class Dependency {
  static final sl = GetIt.instance;
  Dependency._init();

  static Future<void> init() async {
//-------------------------------------------------------//
    sl.registerLazySingleton<LocaleSource>(() => LocaleSourceImpl(sl()));

    sl.registerLazySingleton<LocaleRepository>(
        () => LocaleRepositoryImpl(sl()));
    sl.registerLazySingleton(() => ReadLocaleUsecase(sl()));
    sl.registerLazySingleton(() => SaveLocaleUsecase(sl()));
    sl.registerLazySingleton(() => LocaleCubit(
          readLocaleUsecase: sl(),
          saveLocaleUsecase: sl(),
        ));
    final sharedPref = await SharedPreferences.getInstance();
    sl.registerLazySingleton(() => sharedPref);
    sl.registerLazySingleton(() => Client());

    sl.registerLazySingleton<ConnectionChecker>(
      () => ConnectionCheckerImpl(),
    );
    sl.registerLazySingleton<TokenSource>(() => TokenSourceImpl(sl()));
    sl.registerLazySingleton<HeaderProvider>(() => HeaderProviderImpl());
    sl.registerLazySingleton(() => AuthHeaderProvider(sl()));



//---------------------------Git Repository Start-------------------------------//

    sl.registerLazySingleton<GitRepositoryRemote>(
      () => GitRepositoryRemoteImpl(sl()),
    );

    sl.registerLazySingleton<GitRepositoryRepository>(
      () => GitRepositoryRepositoryImpl(
        sl(),
        sl(),
      ),
    );
    sl.registerLazySingleton(() => GitRepositoryUsecase(sl()));
    sl.registerFactory(() => GitRepositoryCubit(gitRepositoryUsecase: sl()));
    sl.registerFactory(() => ValidationCubit());

//---------------------------Git Repository End-------------------------------//

  }


  static final providers = <BlocProvider>[
    BlocProvider<LocaleCubit>(
      create: (context) => Dependency.sl<LocaleCubit>(),
    ),
    BlocProvider<GitRepositoryCubit>(
      create: (context) => Dependency.sl<GitRepositoryCubit>(),
    ),
    BlocProvider<ValidationCubit>(
      create: (context) => Dependency.sl<ValidationCubit>(),
    ),
  ];
}
