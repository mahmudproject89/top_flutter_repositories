# Flutter GitHub Repositories App

This Flutter application allows users to browse and explore GitHub repositories related to Flutter. Users can view a list of repositories, sort them based on criteria like the last updated date-time or star count, and view detailed information about each repository.

## Features

1. Fetches repository list from GitHub API using "Flutter" as a query keyword.
2. Stores fetched data in a local database to enable offline browsing.
3. Implements pagination for fetching 10 new items per scroll.
4. Refreshes repository list from the API no more frequently than once every 30 minutes.
5. Displays a list of repositories on the home page.
6. Allows sorting of the repository list by the last updated date-time or star count.
7. Persists selected sorting option across app sessions.
8. Provides a repository details page showing owner's name, photo, description, last update date-time, and other relevant details.
9. Saves repository list and repository details data for offline browsing.

## Architecture and Design Patterns

- **MVVM (Model-View-ViewModel)**: Separates concerns and facilitates maintainability.
- **Repository Pattern**: Abstracts data layer for easier testing and scalability.

## State Management

- **Bloc (Business Logic Component)**: Manages application state using Bloc pattern for better organization and separation of concerns.

## Networking and Database

- **HTTP Client**: Utilizes `http` package for API requests.
- **SQFlite**: Stores fetched data locally with SQFlite package.

## Error Handling

- Centralized API call error handling provides meaningful feedback to users in case of network errors.

## Navigation

- Utilizes Flutter's built-in navigation system for seamless navigation between screens.

## Testing

- **Unit Tests**: Ensures correctness of business logic, repositories, and Blocs.
- **Widget Tests**: Validates UI components render correctly.
- **Integration Tests**: Verifies interaction between different layers of the application.

## Future Improvements

- **Authentication**: Implement authentication mechanism for future-proofing.
- **Retry Mechanism**: Add retry mechanism for failed API calls.
- **Multiple Environments**: Setup different app flavors for development, testing, and production.
- **Enhanced UI**: Improve UI/UX design, animations, and user experience.
- **Localization**: Add support for multiple languages.
- **CI/CD**: Set up CI/CD pipelines for automated testing and deployment.

## GitLab Repository

The source code for this project is hosted on GitLab. You can access the repository [here](https://gitlab.com/mahmudproject89/top_flutter_repositories).

## Screenshots

![Splash Screen](screenshots/splash.JPEG)
![Repository Screen](screenshots/repository.JPEG)
![Repository Details Screen](screenshots/repository_details.JPEG)
![Pagination Screen](screenshots/pagination.JPEG)
![Next Refresh Screen](screenshots/next_refresh.JPEG)


[//]: # "Include screenshots of the app here."

## Project Diagram

[//]: # "Include a diagram of the project architecture here."

## Unfinished Tasks

- Authentication mechanism is not implemented.
- Retry mechanism for failed API calls is pending.

## Future Scope

- Refine codebase based on feedback and emerging requirements.
- Implement additional features and enhancements to improve code quality and user experience.
